import { fromJS } from 'immutable';
import  * as Actions from '../actions/coincap';

export const initialState = fromJS({
  dash: '',
  eth: '',
  ltc: '',
  btc: '',
  dashToBtc: '',
  ethToBtc: '',
  ltcToBtc: '',
  processing: false,
  err: false,
  serverData: []
});

export default function coinCap(state = initialState, action) {
  switch (action.type) {
    case Actions.SET_DATA: {
      return state.set('dash', action.payload.dash.price)
      .set('dashToBtc', action.payload.dash.price_btc)
      .set('eth', action.payload.ethereum.price)
      .set('ethToBtc', action.payload.ethereum.price_btc)
      .set('ltc', action.payload.litecoin.price)
      .set('ltcToBtc', action.payload.litecoin.price_btc)
      .set('btc', action.payload.bitcoin.price)
    }
    case Actions.SET_COIN_CAP_SERVER_DATA: {
      return state.set('serverData', action.payload)
    }
    case Actions.SET_DATA_ERR: {
      // return state.err = action.payload;
      return state;
    }
    case Actions.DATA_PROCESSING: {
      return state;
    }
    default:
      return state;
  }
}
