import { fromJS } from 'immutable';
import  * as Actions from '../actions/poliniex';

export const initialState = fromJS({
  poliniex: [],
  processing: false,
  err: false,
  serverData: []
});

export default function poliniex(state = initialState, action) {
  switch (action.type) {
    case Actions.SET_POLINEX_DATA: {
      const relevantTickers = [];
      const obj = action.payload;

      for (var prop in obj) {
        if ((prop === 'BTC_ETH' || prop === 'BTC_LTC' || prop === 'BTC_DASH')) {
          relevantTickers.push( { name: prop, data: obj[`${prop}`] })
        }
      }
      return state.set('poliniex', relevantTickers);
    }
    case Actions.SET_POLINEX_SERVER_DATA: {
      return state.set('serverData', action.payload)
    }
    case Actions.SET_POLINEX_DATA_ERR: {
      console.log('in set data err', action.payload);
      return state;
    }
    case Actions.DATA_PROCESSING: {
      return state;
    }
    default:
      return state;
  }
}
