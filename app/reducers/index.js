import { combineReducers } from 'redux';
import coinCap, { initialState as coinCapInitialState } from './coincap';
import counter, { initialState as counterInitialState } from './counter';
import poliniex, { initialState as poliniexInitialState } from './poliniex';
import err, { initialState as errInitialState } from './err';

const reducers = {
  counter,
  coinCap,
  poliniex,
  err
};

export const initialState = {
  coinCap: coinCapInitialState,
  counter: counterInitialState,
  poliniex: poliniexInitialState,
  err: errInitialState
};

export default combineReducers(reducers);
