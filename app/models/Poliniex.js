const mongoose = require( 'mongoose' );

const PoliniexSchema = new mongoose.Schema({
 data: Array,
 timeStamp: Object
}, {collection: 'PoliniexData'});

mongoose.model( 'PoliniexSchema', PoliniexSchema);
