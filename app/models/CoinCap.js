const mongoose = require( 'mongoose' );

const CoinCapSchema = new mongoose.Schema({
 dash: Object,
 ethereum: Object,
 litecoin: Object,
 bitcoin: Object,
 timeStamp: Object
}, {collection: 'coinCapData'});

mongoose.model( 'CoinCapSchema', CoinCapSchema);
