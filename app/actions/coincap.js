const Promise = require('bluebird');
const request = Promise.promisifyAll(require('request'), { multiArgs: true });
const REST_API_URL = '/api/coinCap';
const REST_API_URL_GET = '/api/coinCap/retrieve';
import axios from 'axios';

export const DATA_PROCESSING = 'DATA_PROCESSING';
export const SET_DATA = 'SET_DATA';
export const SET_COIN_CAP_SERVER_DATA = 'SET_COIN_CAP_SERVER_DATA';
export const SET_DATA_ERR = 'SET_DATA_ERR';

export function getData() {
  return (dispatch) => {
    dispatch(dataProcessing(true));

    var urlList = ['http://coincap.io/page/DASH', 'http://coincap.io/page/ETH', 'http://coincap.io/page/LTC', 'http://coincap.io/page/BTC'];
    Promise.map(urlList, function(url) {
        return request.getAsync(url).spread(function(response,body) {
          return [JSON.parse(body)];
        });
    }).then(function(results) {
      dispatch(setData(results[0], results[1], results[2], results[3]));
      dispatch(postToDb(results[0], results[1], results[2], results[3]));
    }).catch(function(err) {
         dispatch(setDataErr(err));
    });
    dispatch(dataProcessing(false));
  };
}

export function postToDb( dash, ethereum, litecoin, bitcoin ) {

  const body = JSON.stringify({
    dash: dash[0],
    ethereum: ethereum[0],
    litecoin: litecoin[0],
    bitcoin: bitcoin[0]
  })

  return () => {
    axios.post(REST_API_URL, body, {
      headers: {
        'Accept': 'application/json',
        data: body
      }
    })
    .then( res => {
      console.log(res);
    })
    .catch( err => {
      console.warn('err::', err);
    });
  }
}

export function readDataBase() {
  console.log('in read db');

  return (dispatch) => {
    axios.post(REST_API_URL_GET, {obj: 'ect'}, {
      headers: {
        'Accept': 'application/json',
        'data': {obj: 'ect'}
      }
    })
    .then( res => {
      dispatch(setCoinCapServerData(res.data))
      // console.log('%c resss', 'font-size:30px;color:red;', res);
    })
    .catch( err => {
      console.log('%c err', 'font-size:30px;color:red;', err);
    });
  }
}

export function setCoinCapServerData ( data ) {
  return {
    type: SET_COIN_CAP_SERVER_DATA,
    payload: data
  };
}

export function dataProcessing ( processing ) {
  return {
    type: DATA_PROCESSING,
    payload: processing
  };
}

export function setData ( dash, ethereum, litecoin, bitcoin ) {
  return {
    type: SET_DATA,
    payload: { dash: dash[0], ethereum: ethereum[0], litecoin: litecoin[0], bitcoin: bitcoin[0] }
  };
}

export function setDataErr ( err ) {
  return {
    type: SET_DATA_ERR,
    payload: err
  };
}
