import { bindActionCreators } from 'redux';
import * as CounterActions from './counter';
import * as CoinCapActions from './coincap';
import * as PoliniexActions from './poliniex';

export function bindActions (dispatch) {
  return {
    counter: bindActionCreators(CounterActions, dispatch),
    coinCap: bindActionCreators(CoinCapActions, dispatch),
    poliniex: bindActionCreators(PoliniexActions, dispatch)
  };
}
