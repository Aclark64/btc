var Promise = require('bluebird');
const REST_API_URL = '/api/poliniex';
const REST_API_URL_GET = '/api/poliniex/retrieve';
import axios from 'axios';

export const DATA_PROCESSING = 'DATA_PROCESSING';
export const SET_POLINEX_DATA = 'SET_POLINEX_DATA';
export const SET_POLINEX_SERVER_DATA = 'SET_POLINEX_SERVER_DATA';
export const SET_POLINEX_DATA_ERR = 'SET_POLINEX_DATA_ERR';

export function getPoliniexData() {
  return (dispatch) => {
    dispatch(dataProcessing(true));
    Promise.resolve(axios.get('https://poloniex.com/public?command=returnTicker')).then(function(res) {
      return res.data;
    }).then(function(results) {
      dispatch(setPoliniexData(results));
      dispatch(postToDb(results));
    }).catch(function(err) {
      dispatch(setPoliniexDataErr(err));
    });
  };
}

export function postToDb( data ) {

  const relevantTickers = [];
  const obj = data;

  for (var prop in obj) {
    if ((prop === 'BTC_ETH' || prop === 'BTC_LTC' || prop === 'BTC_DASH')) {
      relevantTickers.push( { name: prop, data: obj[`${prop}`] })
    }
  }

  return () => {
    axios.post(REST_API_URL, relevantTickers, {
      headers: {
        'Accept': 'application/json',
        data: JSON.stringify(relevantTickers)
      }
    })
    .then( res => {
      console.log(res);
    })
    .catch( err => {
      console.warn('err::', err);
    });
  }
}

export function readDataBase() {
  return (dispatch) => {
    axios.post(REST_API_URL_GET)
    .then( res => {
      dispatch(setPoliniexServerData(res.data))
      console.log('%c resss', 'font-size:30px;color:red;', res);
    })
    .catch( err => {
      console.warn('err::', err);
    });
  }
}

export function dataProcessing ( processing ) {
  console.log(processing);
  return {
    type: DATA_PROCESSING,
    payload: processing
  };
}

export function setPoliniexData ( data ) {
  return {
    type: SET_POLINEX_DATA,
    payload: data
  };
}

export function setPoliniexServerData ( data ) {
  return {
    type: SET_POLINEX_SERVER_DATA,
    payload: data
  };
}

export function setPoliniexDataErr ( err ) {
  console.log('erererer', err);
  return {
    type: SET_POLINEX_DATA_ERR,
    payload: err
  };
}
