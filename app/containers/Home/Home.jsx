import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActions } from 'actions';
// import autobahn from 'autobahn';
// import axios from 'axios';
// import querystring from 'querystring';
// const KrakenClient = require('kraken-api');
// const kraken = new KrakenClient('K8KdDFWaiM3lrjwUqDF+LONPQhTqT8ypsMl+fctcLql2ueqp2QTZAPTP', 'RTQ1+IAhgLSbgWfo+UALRpOUYbr2UrH/65g/YVB128DLasUCn0iDg3hmzTSleLThjWDrCGEJUNGkAqxYTKuzGA==');

if (process.env.BROWSER) {
  require('./style.scss');
}

// const ticks = [];

class Home extends Component {

  constructor() {
    super();

		this.state = {
			coinCap: {},
      poliniex: [],
		}
  }

	componentDidMount() {
    this.props.actions.coinCap.getData();
		this.props.actions.poliniex.getPoliniexData();
    // this._getKraken();
	}

	componentWillReceiveProps(nextProps) {
		// console.log('nextProps', nextProps.state.poliniex.get('poliniex'));
		if (nextProps.state.coinCap.toJS()) {
			this._renderCoinCap(nextProps.state.coinCap.toJS())
		}
		if (nextProps.state.poliniex.get('poliniex').length) {
			this._renderPoliniex(nextProps.state.poliniex.toJS())
		}
	}

	_renderCoinCap(data) {
		if (!data) {
			return;
		}
		this.setState({
			coinCap: data
		})
	}

  _renderPoliniex(data){
    if (!data) {
			return;
		}
		this.setState({
			poliniex: data.poliniex
		})
  }

  _renderTicks(){
    if (!this.state.poliniex) {
      return;
    }
    return this.state.poliniex.map( (item, index) => {
      return (
        <div key={ index }>
          { item.name } -- { item.data.last }
        </div>
      )
    })
  }

	render () {
    // console.log('this.state', this.state);
		return (
			<div>
        {this.state.coinCap.dash ?
          <div>
            <h1>Hello Donald</h1>
            <h2>Bitcoin price: ${ this.state.coinCap.btc }</h2>
            <h3>CoinCap Prices:</h3>
            <div className='coin-container'>
              <p>Dash: $ { this.state.coinCap.dash }</p>
              <p>Dash to Bitcoin: { this.state.coinCap.dashToBtc }</p>
              <p>Bitcoins of Dash: { this.state.coinCap.btc / this.state.coinCap.dash } </p>
            </div>
            <div className='coin-container'>
              <p>Ethereum: $ { this.state.coinCap.eth }</p>
              <p>Ethereum to Bitcoin: {this.state.coinCap.ethToBtc }</p>
              <p>Bitcoins of Ethereum: {this.state.coinCap.btc / this.state.coinCap.eth } </p>
            </div>
            <div className='coin-container'>
              <p>Litecoin: $ { this.state.coinCap.ltc }</p>
              <p>Litecoin to Bitcoin: { this.state.coinCap.ltcToBtc }</p>
              <p>Bitcoins of Litecoin: { this.state.coinCap.btc / this.state.coinCap.ltc } </p>
            </div>
          </div>
        :
          <div> one sec </div>
        }
        { this.state.poliniex.length ?
          <div>
            <h3>Poliniex Prices:</h3>
            <div className='coin-container'>
              { this._renderTicks() }
            </div>
          </div>
        :
          null
        }

			</div>
		);
	}
}

Home.propTypes = {
	name: PropTypes.string,
  actions: PropTypes.object,
  state: PropTypes.object
};

// export default Home;
export default connect(
  function mapStateToProps(state) { return { state }; },
  function mapDispatchToProps(dispatch) { return { actions: bindActions(dispatch) }; }
)(Home);



  // _getKraken() {
  //   axios.post('https://api.kraken.com/0/public/Ticker/', querystring.stringify({ pair : 'XXBTZUSD' }) , {
  //     headers: {
  //       'Access-Control-Allow-Method': 'PUT',
  //       'Access-Control-Allow-Origin': '*',
  //       'Access-Control-Allow-Headers': 'origin, x-requested-with, content-type',
  //       'Accept': 'application/json'
  //     }
  //   })
  //     .then( res => {
  //       console.log('results::::::', res);
  //     })
  //     .catch( (err) =>  console.warn('err', err) );
  // //   axios.post('https://api.kraken.com/0/public/Ticker/', querystring.stringify({ pair : 'XXBTZUSD' }) , {
  // //   headers: {
  // //     'Accept': 'application/json',
  // //     'Access-Control-Request-Method: POST'
  // //     }
  // //   })
  // //   .then( res => {
  // //     if (res.status >= 400) {
  // //       dispatch( userDataErr(true) );
  // //       return;
  // //     }
  // //     if (res.data.jwt.valid) {
  // //       dispatch(userDataGet( res.data.jwt.token, res.data.result.job_id ));
  // //     }
  // //   })
  // //   .catch( (err) => dispatch( userDataErr( err )) );
  // //   };
  // // }
  // }
