import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActions } from 'actions';

class Timeline extends Component {
  constructor() {
    super();

		this.state = {
			coinCap: [],
      poliniex: [],
		}
  }

  componentDidMount() {
    this.props.actions.coinCap.readDataBase();
		this.props.actions.poliniex.readDataBase();
  }

  componentWillReceiveProps(nextProps) {
		if (nextProps.state.coinCap.get('serverData')) {
      this.setState({ coinCap: nextProps.state.coinCap.get('serverData') })
		}
		if (nextProps.state.poliniex.get('serverData')) {
      this.setState({ poliniex: nextProps.state.poliniex.get('serverData') })
		}
	}

  _renderCoinCap() {
    if (!this.state.coinCap) {
      return;
    }
    return this.state.coinCap.map( (item, index) => {
      return (
        <div key={ index }>
          { item.timeStamp }
          bitcoin price: { item.bitcoin.price }
          dash price: { item.dash.price } ({ item.bitcoin.price / item.dash.price} dash to 1 bitcoin)
          ethereum price: { item.ethereum.price } ({ item.bitcoin.price / item.ethereum.price} ethereum to 1 bitcoin)
          litecoin price: { item.litecoin.price } ({ item.bitcoin.price / item.litecoin.price} litecoin to 1 bitcoin)
        </div>
      )
    })
  }

  _renderPoliniex() {
    if (!this.state.poliniex) {
      return;
    }
    return this.state.poliniex.map( (item, index) => {
      console.log('item', item);
      return (
        <div key={ index }>
          { item.timeStamp }
          :::{ item.data[0].name }:::
          { item.data[0].data.last }
          {'\n'}
          ::::{ item.data[1].name }::::
          { item.data[1].data.last }
          {'\n'}
          ::::{ item.data[2].name }::::
          { item.data[2].data.last }
          {'\n'}
        </div>
      )
    })
  }

  render() {
    // console.log('%c state timeline' , 'font-size:50px', this.state);
    const { increment, decrement } = this.props.actions.counter;
    return (
      <div className="counter">
      <p className="heading-1">{ this.props.counter.get('value', 0) }</p>
      <button onClick={ increment }>Increase</button>
      <button onClick={ decrement }>Decrease</button>
      { this._renderCoinCap() }
      Poliniex
      { this._renderPoliniex() }
      </div>
    );
  }
}

Timeline.propTypes = {
  state: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired,
  counter: PropTypes.object.isRequired
};

export default connect(
  function mapStateToProps(state) { return { state, counter: state.counter }; },
  function mapDispatchToProps(dispatch) { return { actions: bindActions(dispatch) }; }
)(Timeline);
