import path from 'path';
import express from 'express';
import cors from 'cors';
import compression from 'compression';
import helmet from 'helmet';
var mongoose = require( 'mongoose' );
const routes = require('./api.js');
import { get as _get } from 'lodash';
import React from 'react';
import { renderToString } from 'react-dom/server';
import { match, RouterContext } from 'react-router';
import configureStore from '../app/store/configureStore';
import { initialState as initialAppState } from '../app/reducers';
import { getRoutes } from '../app/routes';
import render from './render';


const isDev = process.env.NODE_ENV !== 'production';
const env  = isDev ? 'development' : process.env.NODE_ENV;
const port = process.env.PORT || 8080;
const host = process.env.HOST || '0.0.0.0';

const publicPath = path.resolve(__dirname, '../public');

module.exports = {
  start: function() {
    let server = express();

    server.use(cors());
    server.set('env', env);
    server.set('host', host);
    server.set('port', port);

    // var mongoDB = 'mongodb://127.0.0.1/btcTest';
    var mongoDB = 'mongodb://admin:asdf12345@ds125555.mlab.com:25555/btc';
    mongoose.connect(mongoDB, {
      useMongoClient: true
    });
    var db = mongoose.connection;
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));


    if (isDev) {
      require('./dev.server.js')(server);
    }
    else {
      server.use(compression());
      server.use(helmet());
    }

    server.use(express.static(publicPath));

    server.get('*', function response(req, res) {
      matchRoute(req, res, initialAppState);
    });



    server.post('/api/coinCap/retrieve', routes.getAllCoinCapHandler);
    server.post('/api/coinCap', routes.postCoinCapHandler);
    server.post('/api/poliniex/retrieve', routes.getAllPoliniexHandler);
    server.post('/api/poliniex', routes.postPoliniexHandler);

    server.listen(port, host, function onStart(err) {
      if (err) {
        console.log(err);
      }
      console.log('=========>\n');
      console.info('Server is running %s on %s:%s', env, host, port);
      console.log('\n=========>');
    });
  }
}

function matchRoute (req, res, initialAppState) {
  const store = configureStore( initialAppState );
  const initialState = store.getState();
  const routes = getRoutes(store);

  match({ routes, location: req.url }, (error, redirectLocation, renderProps) => {
    if (error) {
      const status = _get(error, 'status', 500);
      const message = status === 404 ? 'Page could not be found.' : _get(error, 'message', 'An unknown error occurred.');
      const html = render(React)(store, renderProps, {status, message});

      if (req.accepts('html')) {
        res.status(status).send(html);
      }
      else {
        res.status(status).send(message);
      }
    }
    else if (redirectLocation) {
      res.redirect(302, redirectLocation.pathname + redirectLocation.search);
    }
    else {
      const html = render(React)(store, renderProps);
      res.status(200).send(html);
    }
  });
}
