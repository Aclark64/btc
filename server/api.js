var mongoose = require( 'mongoose' );
var CoinCap = require('../app/models/CoinCap')
var CoinCapSchema = mongoose.model( 'CoinCapSchema' );
var poliniex = require('../app/models/Poliniex')
var PoliniexSchema = mongoose.model( 'PoliniexSchema' );

/* ******** ******** ******** ******** ******** ********  */
/* ******** ******** REST API HANDLERS ******** ********  */
/* ******** ******** ******** ******** ******** ********  */
exports.getAllCoinCapHandler = function (req, res){
  console.log('%c in getAllCoinCapHandlergetAllCoinCapHandlergetAllCoinCapHandlergetAllCoinCapHandler', 'font-size:30px;color:green', req, '\n------------\n------------\n', res);
  CoinCapSchema.find({}, function(err, theArray){
    if (!err){
      res.json(theArray);
    }
  });
};

exports.getAllPoliniexHandler = function (req, res){
  PoliniexSchema.find({}, function(err, theArray){
    if (!err){
      res.json(theArray);
    }
  });
};

exports.postCoinCapHandler = function(req, res){
  const coinData = JSON.parse(req.headers.data);

  const newRecord = new CoinCapSchema();
  newRecord.dash = coinData.dash,
  newRecord.ethereum = coinData.ethereum,
  newRecord.litecoin = coinData.litecoin,
  newRecord.bitcoin = coinData.bitcoin,
  newRecord.timeStamp = new Date(),

  newRecord.save(function(err, savedUser){
	  if(err){
	     res.json(false);
	     console.log("❌❌could not be added❌❌");
	   }else{
	     res.json(true);
	    //  console.log(newRecord + " added successfully");
	     console.log("success 👌");
	   }
   });
};

exports.postPoliniexHandler = function(req, res){
  // console.log('reqreqtoString', JSON.parse(req.headers.data));
  //app.post('/api/coin'
  // const coinData = JSON.parse(req.headers.data);

  const newRecord = new PoliniexSchema();
  newRecord.data = JSON.parse(req.headers.data);
  newRecord.timeStamp = new Date();

  newRecord.save(function(err, savedUser){
	  if(err){
	     res.json(false);
	     console.log("❌❌could not be added❌❌");
	   }else{
	     res.json(true);
	     console.log("poliniex success 👌");
	   }
   });
};
